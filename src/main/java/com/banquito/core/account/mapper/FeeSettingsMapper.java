package com.banquito.core.account.mapper;

import com.banquito.core.account.dto.FeeSettingsDTO;
import com.banquito.core.account.model.FeeSettings;
import com.banquito.core.account.model.FeeSettingsPK;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FeeSettingsMapper {

  public static FeeSettings buildFeeSettings(FeeSettingsDTO dto) {
    return FeeSettings.builder()
        .pk(
            FeeSettingsPK.builder()
                .feeName(dto.getFeeName())
                .typeId(dto.getTypeId())
                .familyId(dto.getFamilyId())
                .build())
        .calculationType(dto.getCalculationType())
        .applicationFrecuency(dto.getApplicationFrecuency())
        .amount(dto.getAmount())
        .trigger(dto.getTrigger())
        .applicationStartDate(dto.getApplicationStartDate())
        .condition(dto.getCondition())
        .formula(dto.getFormula())
        .build();
  }

  public static FeeSettingsDTO buildFeeSettingsDTO(FeeSettings feeSettings) {
    return FeeSettingsDTO.builder()
        .feeName(feeSettings.getPk().getFeeName())
        .typeId(feeSettings.getPk().getTypeId())
        .familyId(feeSettings.getPk().getFamilyId())
        .calculationType(feeSettings.getCalculationType())
        .applicationFrecuency(feeSettings.getApplicationFrecuency())
        .amount(feeSettings.getAmount())
        .trigger(feeSettings.getTrigger())
        .applicationStartDate(feeSettings.getApplicationStartDate())
        .condition(feeSettings.getCondition())
        .formula(feeSettings.getFormula())
        .build();
  }
}
