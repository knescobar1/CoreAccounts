package com.banquito.core.account.dao;

import com.banquito.core.account.model.TaxSettings;
import com.banquito.core.account.model.TaxSettingsPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaxSettingsRepository extends JpaRepository<TaxSettings, TaxSettingsPK> {}
