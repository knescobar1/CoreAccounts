package com.banquito.core.account.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class AccountFamilyDTO {
  private String familyId; // Used for account creation.

  private String name;

  private String holderType;

  private BigDecimal recommendedDepositAmount;

  private BigDecimal maxDepositAmount;

  private BigDecimal maxWithdrawalAmount;

  private BigDecimal minWithdrawalAmount;

  private String allowsCheckbook;

  private String allowsOverdraft;

  private String paysInterest;

  private String[] currencies;

  private String memo;

  private List<FeeSettingsDTO> feeDTOs;

  private List<InterestSettingsDTO> interestDTOs;

  private List<TaxSettingsDTO> taxDTOs;
}
