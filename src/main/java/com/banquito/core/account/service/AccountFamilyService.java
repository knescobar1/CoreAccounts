package com.banquito.core.account.service;

import com.banquito.core.account.dao.AccountFamilyRepository;
import com.banquito.core.account.dao.FeeSettingsRepository;
import com.banquito.core.account.dao.InterestSettingsRepository;
import com.banquito.core.account.dao.TaxSettingsRepository;
import com.banquito.core.account.enums.AccountStateEnum;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.model.AccountFamily;
import com.banquito.core.account.model.AccountFamilyPK;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountFamilyService {

  private final AccountFamilyRepository accountFamilyRepository;
  private final InterestSettingsRepository interestSettingsRepository;
  private final FeeSettingsRepository feeSettingsRepository;
  private final TaxSettingsRepository taxSettingsRepository;

  public void create(AccountFamily accountFamily) {
    accountFamily.setState(AccountStateEnum.ACTIVE.getValue());
    accountFamily.setCreationDate(new Date());
    this.accountFamilyRepository.save(accountFamily);
    if (!accountFamily.getInterests().isEmpty()) {
      this.interestSettingsRepository.saveAll(accountFamily.getInterests());
    }
    if (!accountFamily.getFees().isEmpty()) {
      this.feeSettingsRepository.saveAll(accountFamily.getFees());
    }
    if (!accountFamily.getTaxes().isEmpty()) {
      this.taxSettingsRepository.saveAll(accountFamily.getTaxes());
    }
  }

  public List<AccountFamily> searchByTypeId(String typeId) {
    return this.accountFamilyRepository.findByPkTypeId(typeId);
  }

  public AccountFamily getByCode(AccountFamilyPK code) {
    Optional<AccountFamily> accountFamilyOpt = this.accountFamilyRepository.findById(code);
    return accountFamilyOpt.orElseThrow(
        () -> new NotFoundException("The specified account does not exist"));
  }

  public void modify(AccountFamily accountFamily) {
    AccountFamily accountFamilyDB = this.getByCode(accountFamily.getPk());

    accountFamilyDB.setHolderType(accountFamily.getHolderType());
    accountFamilyDB.setAllowsCheckbook(accountFamily.getAllowsCheckbook());
    accountFamilyDB.setAllowsOverdraft(accountFamily.getAllowsOverdraft());
    accountFamilyDB.setPaysInterest(accountFamily.getPaysInterest());
    accountFamilyDB.setState(accountFamily.getState());
    accountFamilyDB.setLastModifiedDate(new Date());
    accountFamilyDB.setMaxWithdrawalAmount(accountFamily.getMaxWithdrawalAmount());
    accountFamilyDB.setMinWithdrawalAmount(accountFamily.getMinWithdrawalAmount());
    this.accountFamilyRepository.save(accountFamilyDB);
  }
}
