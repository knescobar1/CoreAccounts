package com.banquito.core.account.dao;

import com.banquito.core.account.model.AccountFamily;
import com.banquito.core.account.model.AccountFamilyPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AccountFamilyRepository extends JpaRepository<AccountFamily, AccountFamilyPK> {
  List<AccountFamily> findByPkTypeId(String typeId);

  Optional<AccountFamily> findByPkFamilyId(String familyId);
}
