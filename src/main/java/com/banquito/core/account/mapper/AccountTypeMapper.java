package com.banquito.core.account.mapper;

import com.banquito.core.account.dto.AccountTypeDTO;
import com.banquito.core.account.model.AccountType;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountTypeMapper {

    public static AccountType buildAccountType (AccountTypeDTO dto){
        return AccountType.builder()
                .name(dto.getName())
                .notes(dto.getNotes())
                .accrual(dto.getAccrual())
                .build();
    }
    public static AccountTypeDTO buildAccountTypeDTO(AccountType accountType){
        return AccountTypeDTO.builder()
                .name(accountType.getName())
                .notes(accountType.getNotes())
                .accrual(accountType.getAccrual())
                .build();
    }

}
