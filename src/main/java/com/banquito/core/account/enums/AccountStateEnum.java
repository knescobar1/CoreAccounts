package com.banquito.core.account.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum AccountStateEnum {
  DRAFT("DRAFT", "Draft"),
  ACTIVE("ACTIVE", "Active"),
  INACTIVE("INACTIVE", "Inactive");

  private final String value;
  private final String text;
}
