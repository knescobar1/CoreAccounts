package com.banquito.core.account.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

public class KafkaCollTopicConfig {
  @Bean
  public NewTopic banquitoCollTopic() {
    return TopicBuilder.name("transaction_wesos").build();
  }
}
