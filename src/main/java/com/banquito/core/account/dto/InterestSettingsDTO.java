package com.banquito.core.account.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InterestSettingsDTO {

  private String interestId;

  private String typeId;

  private String familyId;

  private String frecuency;

  private BigDecimal rate;

  private String rateTerms;

  private String rateUnit;

  private BigDecimal spread;
}
