package com.banquito.core.account.service;

import com.banquito.core.account.dao.FeeSettingsRepository;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.model.FeeSettings;
import com.banquito.core.account.model.FeeSettingsPK;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FeeSettingsService {
  private final FeeSettingsRepository feeSettingsRepository;

  public FeeSettings create(FeeSettings feeSettings) {
    this.feeSettingsRepository.save(feeSettings);
    return feeSettings;
  }

  public FeeSettings searchById(FeeSettingsPK feeSettingsPK) {
    Optional<FeeSettings> feeSettingOpt = this.feeSettingsRepository.findById(feeSettingsPK);
    return feeSettingOpt.orElseThrow(() -> new NotFoundException("The Interest type is not found"));
  }

  public FeeSettings modify(FeeSettings feeSettings) {
    FeeSettings feeSettingDB = this.searchById(feeSettings.getPk());

    feeSettingDB.setCalculationType(feeSettings.getCalculationType());
    feeSettingDB.setApplicationFrecuency(feeSettings.getApplicationFrecuency());
    feeSettingDB.setAmount(feeSettings.getAmount());
    feeSettingDB.setTrigger(feeSettings.getTrigger());
    feeSettingDB.setApplicationStartDate(feeSettings.getApplicationStartDate());
    feeSettingDB.setCondition(feeSettings.getCondition());
    feeSettingDB.setFormula(feeSettings.getFormula());
    this.feeSettingsRepository.save(feeSettingDB);
    return feeSettingDB;
  }

  public void delete(FeeSettingsPK feeSettingsPK) {
    this.feeSettingsRepository.deleteById(feeSettingsPK);
  }
}
