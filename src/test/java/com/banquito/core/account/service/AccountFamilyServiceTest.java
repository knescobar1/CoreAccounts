package com.banquito.core.account.service;

import com.banquito.core.account.dao.AccountFamilyRepository;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.model.AccountFamily;
import com.banquito.core.account.model.AccountFamilyPK;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountFamilyServiceTest {

  @InjectMocks private AccountFamilyService accountFamilyService;

  @Mock private AccountFamilyRepository accountFamilyRepository;

  private AccountFamily accountFamily;
  private AccountFamilyPK pk;

  @BeforeEach
  void setUp() {
    pk = new AccountFamilyPK("type1", "Family1");

    accountFamily =
        AccountFamily.builder()
            .pk(pk)
            .name("Cuenta Ahorros")
            .holderType("Holder 1")
            .allowsCheckbook("T")
            .paysInterest("T")
            .interests(List.of())
            .fees(List.of())
            .taxes(List.of())
            .build();
  }

  @Test
  void whenCreate_thenStateAndCreationDateShouldBeSet() {
    when(accountFamilyRepository.save(any())).thenReturn(accountFamily);

    accountFamilyService.create(accountFamily);

    String expectedState = "ACTIVE";

    assertAll(
        () -> assertNotNull(accountFamily.getCreationDate()),
        () -> assertEquals(expectedState, accountFamily.getState()));
  }

  @Test
  void whenSearchByTypeId_thenAssertNotNull() {
    when(accountFamilyRepository.findByPkTypeId(any())).thenReturn(List.of());

    List<AccountFamily> accountFamilies = accountFamilyService.searchByTypeId(pk.getTypeId());

    assertNotNull(accountFamilies);
  }

  @Test
  void whenGetByCodeFound_thenAccountFamilyShouldNotBeNull() {
    when(accountFamilyRepository.findById(any())).thenReturn(Optional.of(accountFamily));

    AccountFamily af = accountFamilyService.getByCode(pk);

    assertNotNull(af);
  }

  @Test
  void whenGetByCodeNotFound_thenRaiseNotFoundException() {
    when(accountFamilyRepository.findById(any())).thenReturn(Optional.empty());

    NotFoundException exception =
        assertThrows(NotFoundException.class, () -> accountFamilyService.getByCode(pk));

    assertNotNull(exception);
  }
}
