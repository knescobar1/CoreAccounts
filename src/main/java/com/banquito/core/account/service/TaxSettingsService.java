package com.banquito.core.account.service;

import com.banquito.core.account.dao.TaxSettingsRepository;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.model.TaxSettings;
import com.banquito.core.account.model.TaxSettingsPK;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TaxSettingsService {
  private final TaxSettingsRepository taxSettingsRepository;

  public TaxSettings create(TaxSettings taxSettings) {
    this.taxSettingsRepository.save(taxSettings);
    return taxSettings;
  }

  public TaxSettings searchById(TaxSettingsPK taxSettingsPK) {
    Optional<TaxSettings> taxSettingOpt = this.taxSettingsRepository.findById(taxSettingsPK);
    return taxSettingOpt.orElseThrow(() -> new NotFoundException("The Interest type is not found"));
  }

  public TaxSettings modify(TaxSettings taxSetting) {
    TaxSettings taxSettingDB = this.searchById(taxSetting.getPk());
    taxSettingDB.setRate(taxSetting.getRate());
    taxSettingDB.setApplicationFrecuency(taxSetting.getApplicationFrecuency());
    taxSettingDB.setTrigger(taxSetting.getTrigger());
    taxSettingDB.setFormula(taxSetting.getFormula());
    taxSettingDB.setApplicationStartDate(taxSetting.getApplicationStartDate());
    this.taxSettingsRepository.save(taxSettingDB);
    return taxSettingDB;
  }

  public void delete(TaxSettingsPK taxSettingsPK) {
    this.taxSettingsRepository.deleteById(taxSettingsPK);
  }
}
