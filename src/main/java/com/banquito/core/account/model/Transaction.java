package com.banquito.core.account.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "ACC_TRANSACTION")
@AllArgsConstructor
@Builder
public class Transaction implements Serializable {

  private static final long serialVersionUID = 565646461611L;

  @Id
  @Column(name = "TRANSACTION_ID", nullable = false)
  private String id;

  @Column(name = "CRE_ACCOUNT_ID")
  private String creditorAccountId;

  @Column(name = "CRE_ACCOUNT_TYPE_ID")
  private String creditorAccountTypeId;

  @Column(name = "CRE_ACCOUNT_FAMILY_ID")
  private String creditorAccountFamilyId;

  @Column(name = "CRE_GROUP_INTERNAL_ID")
  private String creditorGroupInternalId;

  @Column(name = "DEB_ACCOUNT_ID")
  private String debtorAccountId;

  @Column(name = "DEB_ACCOUNT_TYPE_ID")
  private String debtorAccountTypeId;

  @Column(name = "DEB_ACCOUNT_FAMILY_ID")
  private String debtorAccountFamilyId;

  @Column(name = "DEB_GROUP_INTERNAL_ID")
  private String debtorGroupInternalId;

  @Column(name = "CREATION_DATE", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  @Column(name = "SERVICE_LEVEL", length = 64)
  private String serviceLevel;

  @Column(name = "AMOUNT", precision = 22, scale = 6, nullable = false)
  private BigDecimal amount;

  @Column(name = "STATE", length = 16, nullable = false)
  private String state;

  @Column(name = "CHANNEL", length = 32)
  private String channel;

  @Column(name = "TRANSACTION_TYPE", length = 3, nullable = false)
  private String transactionType;

  @Column(name = "EXTERNAL_OPERATION_ID")
  private String externalOperation;

  @Column(name = "REFERENCE", length = 128)
  private String reference;

  @Column(name = "DOCUMENT_NUMBER")
  private String documentNumber;

  @Column(name = "TRANASACTION_NUMBER")
  private String transactionNumber;
}
