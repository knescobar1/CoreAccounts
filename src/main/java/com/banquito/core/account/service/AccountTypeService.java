package com.banquito.core.account.service;

import com.banquito.core.account.dao.AccountTypeRepository;
import com.banquito.core.account.model.AccountType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountTypeService {

  private final AccountTypeRepository accountTypeRepository;

  public AccountType findById(String id) {
    Optional<AccountType> accountTypeOpt = this.accountTypeRepository.findById(id);
    if (accountTypeOpt.isPresent()) {
      return accountTypeOpt.get();
    } else {
      return null;
    }
  }

  public AccountType create(AccountType accountType) {

    return this.accountTypeRepository.save(accountType);

  }

  public AccountType modify(AccountType accountType) {
    
    AccountType accountTypeDB = this.findById(accountType.getId());
    accountTypeDB.setName(accountType.getName());
    accountTypeDB.setAccrual(accountType.getAccrual());
    accountTypeDB.setTerm(accountType.getTerm());
    accountTypeDB.setNotes(accountType.getNotes());
    return this.accountTypeRepository.save(accountTypeDB);
  }
}
