package com.banquito.core.account.resource;

import com.banquito.core.account.dto.FeeSettingsDTO;
import com.banquito.core.account.mapper.FeeSettingsMapper;
import com.banquito.core.account.model.FeeSettings;
import com.banquito.core.account.model.FeeSettingsPK;
import com.banquito.core.account.service.FeeSettingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/feeSettings")
@RequiredArgsConstructor
public class FeeSettingsResource {

  private final FeeSettingsService service;

  @PostMapping
  public ResponseEntity<FeeSettingsDTO> create(@RequestBody FeeSettingsDTO dto) {
    try {
      FeeSettings feeSettings = this.service.create(FeeSettingsMapper.buildFeeSettings(dto));
      return ResponseEntity.ok(FeeSettingsMapper.buildFeeSettingsDTO(feeSettings));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping
  public ResponseEntity<FeeSettingsDTO> modify(@RequestBody FeeSettingsDTO dto) {
    try {
      FeeSettings feeSettings = this.service.modify(FeeSettingsMapper.buildFeeSettings(dto));
      return ResponseEntity.ok(FeeSettingsMapper.buildFeeSettingsDTO(feeSettings));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @DeleteMapping("/{feeName}/{typeId}/{familyId}")
  public ResponseEntity<FeeSettingsPK> delete(
      @PathVariable String feeName, @PathVariable String typeId, @PathVariable String familyId) {
    try {
      FeeSettingsPK pk =
          FeeSettingsPK.builder().feeName(feeName).typeId(typeId).familyId(familyId).build();
      this.service.delete(pk);
      return ResponseEntity.ok(pk);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }
}
