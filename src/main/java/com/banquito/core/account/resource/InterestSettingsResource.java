package com.banquito.core.account.resource;

import com.banquito.core.account.dto.InterestSettingsDTO;
import com.banquito.core.account.mapper.InterestSettingsMapper;
import com.banquito.core.account.model.InterestSettings;
import com.banquito.core.account.model.InterestSettingsPK;
import com.banquito.core.account.service.InterestSettingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/interestSettings")
@RequiredArgsConstructor
public class InterestSettingsResource {

  private final InterestSettingsService service;

  @PostMapping
  public ResponseEntity<InterestSettingsDTO> create(@RequestBody InterestSettingsDTO dto) {
    try {
      InterestSettings interestSettings =
          this.service.create(InterestSettingsMapper.buildInterestSettings(dto));
      return ResponseEntity.ok(InterestSettingsMapper.buildInterestSettingsDTO(interestSettings));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping
  public ResponseEntity<InterestSettingsDTO> modify(@RequestBody InterestSettingsDTO dto) {
    try {
      InterestSettings interestSettings =
          this.service.modifySettings(InterestSettingsMapper.buildInterestSettings(dto));
      return ResponseEntity.ok(InterestSettingsMapper.buildInterestSettingsDTO(interestSettings));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @DeleteMapping("/{interestId}/{typeId}/{familyId}")
  public ResponseEntity<InterestSettingsPK> delete(
      @PathVariable String interestId, @PathVariable String typeId, @PathVariable String familyId) {
    try {
      InterestSettingsPK pk =
          InterestSettingsPK.builder().id(interestId).typeId(typeId).familyId(familyId).build();
      this.service.delete(pk);
      return ResponseEntity.ok(pk);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }
}
