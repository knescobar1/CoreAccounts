package com.banquito.core.account.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum TransactionStateEnum {
  EXECUTED("EXECUTED", "Ejecutado"),
  REJECTED("REJECTED", "Rechazado");

  private final String value;
  private final String text;
}
