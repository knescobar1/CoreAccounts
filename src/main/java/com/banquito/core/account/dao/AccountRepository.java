package com.banquito.core.account.dao;

import com.banquito.core.account.model.Account;
import com.banquito.core.account.model.AccountPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, AccountPK> {

  Optional<Account> findByPkId(String accountId);

  Optional<Account> findByAccountNumber(BigInteger accountNumber);

  List<Account> findByAccountNumberIn(List<BigInteger> accountNumbers);

  List<Account> findByPkCustomerGroupId(String customerGroupId);

  List<Account> findByPkCustomerGroupIdAndState(String customerGroupId, String state);

  List<Account> findByPkCustomerGroupIdIn(List<String> internalGroupIds);

  Optional<Account> findByPkCustomerGroupIdAndAccountFamilyNameAndAccountNumber(
      String groupId, String accountType, BigInteger accountNumber);

  Optional<Account> findByPkCustomerGroupIdAndAccountFamilyPkFamilyId(
      String customerGroupId, String familyId);
}
