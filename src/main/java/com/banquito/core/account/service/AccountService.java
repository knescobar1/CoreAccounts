package com.banquito.core.account.service;

import com.banquito.core.account.dao.AccountFamilyRepository;
import com.banquito.core.account.dao.AccountRepository;
import com.banquito.core.account.dao.AccountTypeRepository;
import com.banquito.core.account.dto.AccountDTO;
import com.banquito.core.account.enums.AccountStateEnum;
import com.banquito.core.account.exception.ConflictException;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.mapper.AccountMapper;
import com.banquito.core.account.model.Account;
import com.banquito.core.account.model.AccountFamily;
import com.banquito.core.account.model.AccountPK;
import com.banquito.core.account.model.AccountType;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {
  private final AccountRepository accountRepository;
  private final AccountFamilyRepository accountFamilyRepository;
  private final AccountTypeRepository accountTypeRepository;

  private static final Integer LIMIT_ACCOUNTS = 2;

  public AccountDTO findByAccountNumber(BigInteger accountNumber) {
    Account account =
        this.accountRepository
            .findByAccountNumber(accountNumber)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No se ha encontrado una cuenta con el número " + accountNumber));
    AccountType accountType =
        this.accountTypeRepository
            .findById(account.getPk().getTypeId())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No se ha encontrado el tipo de la cuenta de numero" + accountNumber));
    AccountDTO responseAccount = AccountMapper.buildAccountDTO(account);
    responseAccount.setAccountType(accountType.getName());
    return responseAccount;
  }

  public Account findGroupAccount(String groupInternalId, String type, BigInteger number) {
    return this.accountRepository
        .findByPkCustomerGroupIdAndAccountFamilyNameAndAccountNumber(groupInternalId, type, number)
        .orElseThrow(
            () ->
                new NotFoundException(
                    "El grupo no tiene una cuenta de tipo " + type + "  con el número " + number));
  }

  public Account create(Account account) {
    List<Account> accountsDB = listByGroup(account.getPk().getCustomerGroupId());
    if (accountsDB.size() == LIMIT_ACCOUNTS) {
      throw new ConflictException(
          "No se ha podido crear la cuenta, no se permiten más de dos cuentas por grupo.");
    }
    AccountFamily accountFamily =
        this.accountFamilyRepository
            .findByPkFamilyId(account.getPk().getFamilyId())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No existe una familia de cuentas con el id "
                            + account.getPk().getFamilyId()));

    AccountPK newAccountPK =
        AccountPK.builder()
            .id(generateAccountId())
            .familyId(account.getPk().getFamilyId())
            .typeId(account.getPk().getTypeId())
            .customerGroupId(account.getPk().getCustomerGroupId())
            .build();

    Account newAccount =
        Account.builder()
            .pk(newAccountPK)
            .accountNumber(generateNewAccountNumber())
            .creationDate(new Date())
            .openingDate(new Date())
            .accountFamily(accountFamily)
            .balance(BigDecimal.ZERO)
            .state(AccountStateEnum.DRAFT.getValue())
            .build();

    if (account.getMaturityDate() != null) {
      newAccount.setMaturityDate(account.getMaturityDate());
    }

    return this.accountRepository.save(newAccount);
  }

  private BigInteger generateNewAccountNumber() {
    String id = RandomStringUtils.randomNumeric(15);
    return new BigInteger(id);
  }

  private String generateAccountId() {
    return UUID.randomUUID().toString().substring(0, 32);
  }

  public Account getByAccountId(String accountId) {
    return this.accountRepository
        .findByPkId(accountId)
        .orElseThrow(() -> new NotFoundException("La cuenta con id " + accountId + " no existe."));
  }

  public Account activateAccount(String accountId) {
    Account accountDB = this.getByAccountId(accountId);

    if (accountDB.getState().equals(AccountStateEnum.ACTIVE.getValue())) {
      throw new NotFoundException("No se ha podido activar la cuenta porque ya está activa.");
    }

    accountDB.setState(AccountStateEnum.ACTIVE.getValue());
    accountDB.setOpeningDate(new Date());
    accountDB.setLastModifiedDate(new Date());

    this.accountRepository.save(accountDB);
    return accountDB;
  }

  public Account disableAccount(String accountId) {
    Account accountDB = this.getByAccountId(accountId);

    boolean isInactiveAccount = accountDB.getState().equals(AccountStateEnum.INACTIVE.getValue());
    if (isInactiveAccount) {
      throw new NotFoundException("No se ha podido desactivar la cuenta porque ya está inactiva.");
    }

    accountDB.setState(AccountStateEnum.INACTIVE.getValue());
    accountDB.setLastModifiedDate(new Date());
    accountDB.setClosedDate(new Date());

    this.accountRepository.save(accountDB);

    return accountDB;
  }

  public List<Account> listByGroup(String groupId) {
    return this.accountRepository.findByPkCustomerGroupId(groupId);
  }

  public List<Account> listByGroupAndStateActives(String groupId) {
    return this.accountRepository.findByPkCustomerGroupIdAndState(groupId,AccountStateEnum.ACTIVE.getValue());
  }
}
