package com.banquito.core.account.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

public class KafkaCollRTopicConfig {
  @Bean
  public NewTopic banquitoCollRTopic() {
    return TopicBuilder.name("transaction_recurrement").build();
  }
}
