package com.banquito.core.account.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountTypeDTO {

    private String name;

    private String accrual;

    private String term;

    private String notes;
}
