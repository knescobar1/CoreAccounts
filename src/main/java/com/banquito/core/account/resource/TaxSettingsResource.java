package com.banquito.core.account.resource;

import com.banquito.core.account.dto.TaxSettingsDTO;
import com.banquito.core.account.mapper.TaxSettingsMapper;
import com.banquito.core.account.model.TaxSettings;
import com.banquito.core.account.model.TaxSettingsPK;
import com.banquito.core.account.service.TaxSettingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/taxSettings")
@RequiredArgsConstructor
public class TaxSettingsResource {

  private final TaxSettingsService service;

  @PostMapping
  public ResponseEntity<TaxSettingsDTO> create(@RequestBody TaxSettingsDTO dto) {
    try {
      TaxSettings taxSettings = this.service.create(TaxSettingsMapper.buildTaxSettings(dto));
      return ResponseEntity.ok(TaxSettingsMapper.buildTaxSettingsDTO(taxSettings));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping
  public ResponseEntity<TaxSettingsDTO> modify(@RequestBody TaxSettingsDTO dto) {
    try {
      TaxSettings taxSettings = this.service.modify(TaxSettingsMapper.buildTaxSettings(dto));
      return ResponseEntity.ok(TaxSettingsMapper.buildTaxSettingsDTO(taxSettings));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @DeleteMapping("/{taxName}/{typeId}/{familyId}")
  public ResponseEntity<TaxSettingsPK> delete(
      @PathVariable String taxName, @PathVariable String typeId, @PathVariable String familyId) {
    try {
      TaxSettingsPK pk =
          TaxSettingsPK.builder().taxName(taxName).typeId(typeId).familyId(familyId).build();
      this.service.delete(pk);
      return ResponseEntity.ok(pk);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }
}
