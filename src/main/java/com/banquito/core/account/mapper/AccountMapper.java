package com.banquito.core.account.mapper;

import com.banquito.core.account.dto.AccountDTO;
import com.banquito.core.account.model.Account;
import com.banquito.core.account.model.AccountPK;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountMapper {

  public static Account buildAccount(AccountDTO dto) {
    AccountPK pk =
        AccountPK.builder()
            .familyId(dto.getFamilyId())
            .typeId(dto.getAccountType())
            .customerGroupId(dto.getCustomerGroupId())
            .build();

    return Account.builder()
        .pk(pk)
        .accountNumber(dto.getAccountNumber())
        .openingDate(dto.getOpeningDate())
        .maturityDate(dto.getMaturityDate())
        .build();
  }

  public static AccountDTO buildAccountDTO(Account account) {

    return AccountDTO.builder()
        .accountNumber(account.getAccountNumber())
        .accountType(account.getAccountFamily().getName())
        .familyId(account.getAccountFamily().getPk().getFamilyId())
        .customerGroupId(account.getPk().getCustomerGroupId())
        .openingDate(account.getOpeningDate())
        .maturityDate(account.getMaturityDate())
        .balance(account.getBalance())
        .state(account.getState())
        .build();
  }
}
