package com.banquito.core.account.service;

import com.banquito.cmpaymentscollections.payments.dto.DepositDTO;
import com.banquito.core.account.dao.AccountRepository;
import com.banquito.core.account.dao.TransactionRepository;
import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.core.account.dto.WithdrawalDTO;
import com.banquito.core.account.exception.NotEnoughFundsException;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.exception.TransactionException;
import com.banquito.core.account.mapper.TransactionMapper;
import com.banquito.core.account.model.Account;
import com.banquito.core.account.model.AccountFamily;
import com.banquito.core.account.model.Transaction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionService {

  private final AccountRepository accountRepository;
  private final TransactionRepository transactionRepository;
  private final KafkaTemplate<String, Object> kafkaTemplate;

  public Transaction makeWithdrawal(WithdrawalDTO withdrawalDTO) {
    Account account = this.findAccountByAccountNumber(withdrawalDTO.getAccountNumber());
    AccountFamily family = account.getAccountFamily();

    this.validateAccountFunds(account, withdrawalDTO.getAmount());
    this.validateWithdrawAmountAgainstFamily(withdrawalDTO.getAmount(), family);

    this.withdrawAmountFromAccount(withdrawalDTO.getAmount(), account);

    log.info("Withdrawn {} from {}", withdrawalDTO.getAmount(), account.getAccountNumber());
    // TODO: [ADD] serviceLevel, externalOperation
    Transaction transaction = TransactionMapper.buildWithdrawTransaction(withdrawalDTO, account);

    return this.transactionRepository.save(transaction);
  }

  public List<DepositDTO> publish(List<DepositDTO> deposit) {
    log.info(deposit.toString());
    return deposit;
  }

  public Transaction makeDeposit(DepositDTO depositDTO) {

    Account account = this.findAccountByAccountNumber(depositDTO.getAccountNumber());
    AccountFamily family = account.getAccountFamily();

    this.validateDepositAmountAgainstFamily(depositDTO.getAmount(), family);

    this.depositAmountIntoAccount(depositDTO.getAmount(), account);

    log.info("Deposited {} to {}", depositDTO.getAmount(), account.getAccountNumber());
    // TODO: [ADD] serviceLevel, externalOperation
    Transaction transaction = TransactionMapper.buildTransactionFromDepositDTO(depositDTO, account);

    return this.transactionRepository.save(transaction);
  }

  public Transaction makeTransfer(TransactionDTO transactionDTO) {
    Account creditorAccount =
        this.findAccountByAccountNumber(transactionDTO.getCreditorAccountNumber());
    Account debtorAccount =
        this.findAccountByAccountNumber(transactionDTO.getDebtorAccountNumber());
    AccountFamily debtorFamily = creditorAccount.getAccountFamily();
    AccountFamily creditorFamily = debtorAccount.getAccountFamily();

    this.validateAccountFunds(debtorAccount, transactionDTO.getAmount());
    this.validateWithdrawAmountAgainstFamily(transactionDTO.getAmount(), debtorFamily);
    this.validateDepositAmountAgainstFamily(transactionDTO.getAmount(), creditorFamily);

    this.withdrawAmountFromAccount(transactionDTO.getAmount(), debtorAccount);
    this.depositAmountIntoAccount(transactionDTO.getAmount(), creditorAccount);

    log.info(
        "Transferred {} from {} to {}",
        transactionDTO.getAmount(),
        debtorAccount.getAccountNumber(),
        creditorAccount.getAccountNumber());

    Transaction transferTransaction =
        TransactionMapper.buildTransferTransaction(transactionDTO, creditorAccount, debtorAccount);
    transferTransaction.setId(UUID.randomUUID().toString());
    transferTransaction.setCreationDate(new Date());

    return this.transactionRepository.save(transferTransaction);
  }

  public Transaction makeTransferRecurrement(TransactionDTO transactionDTO) {
    Account creditorAccount =
        this.findAccountByAccountNumber(transactionDTO.getCreditorAccountNumber());
    Account debtorAccount =
        this.findAccountByAccountNumber(transactionDTO.getDebtorAccountNumber());
    AccountFamily debtorFamily = creditorAccount.getAccountFamily();
    AccountFamily creditorFamily = debtorAccount.getAccountFamily();

    transactionDTO.setAmount(this.obtainMontToPay(debtorAccount, transactionDTO.getAmount()));
    this.validateWithdrawAmountAgainstFamily(transactionDTO.getAmount(), debtorFamily);
    this.validateDepositAmountAgainstFamily(transactionDTO.getAmount(), creditorFamily);

    this.withdrawAmountFromAccount(transactionDTO.getAmount(), debtorAccount);
    this.depositAmountIntoAccount(transactionDTO.getAmount(), creditorAccount);

    log.info(
        "Transferred {} from {} to {}",
        transactionDTO.getAmount(),
        debtorAccount.getAccountNumber(),
        creditorAccount.getAccountNumber());

    Transaction transferTransaction =
        TransactionMapper.buildTransferTransaction(transactionDTO, creditorAccount, debtorAccount);
    transferTransaction.setId(UUID.randomUUID().toString());
    transferTransaction.setCreationDate(new Date());

    return this.transactionRepository.save(transferTransaction);
  }

  @KafkaListener(
      topics = "collections_wesos",
      groupId = "fooColl",
      containerFactory = "fooCollListener")
  public void makeOneTransaction(TransactionDTO transactionDTO) {
      log.info(transactionDTO.toString());
      Transaction transactionBD = this.makeTransfer(transactionDTO);
    this.kafkaTemplate.send("transaction_wesos", TransactionMapper.buildTransactionDTO(transactionBD));
  }

  @KafkaListener(
      topics = "collections_recurrement",
      groupId = "fooCollR",
      containerFactory = "fooCollRListener")
  public void makeTransactionRecurrement(TransactionDTO transactionDTO) {
      log.info(transactionDTO.toString());
      Transaction transactionBD = this.makeTransferRecurrement(transactionDTO);
    this.kafkaTemplate.send("transaction_recurrement", transactionDTO);
  }

  @KafkaListener(topics = "payments_pato", groupId = "foo", containerFactory = "fooListener")
  public void makeMultipleDeposits(List<DepositDTO> depositsDTOs) {
    List<Transaction> pendingTransactions = new ArrayList<>();
    List<BigInteger> creditorAccountNumbers = new ArrayList<>();

    for (DepositDTO dto : depositsDTOs) {
      creditorAccountNumbers.add(dto.getAccountNumber());
    }

    List<Account> creditorAccounts = this.findAccountsByAccountNumbers(creditorAccountNumbers);
    for (int i = 0; i < creditorAccounts.size(); i++) {
      Account creditorAccount = creditorAccounts.get(i);
      DepositDTO depositDTO = depositsDTOs.get(i);

      BigDecimal amount = depositDTO.getAmount();
      BigDecimal previousAmount = creditorAccount.getBalance();

      creditorAccount.setBalance(previousAmount.add(amount));

      log.info("Deposited {} to {}", depositDTO.getAmount(), creditorAccount.getAccountNumber());

      pendingTransactions.add(
          TransactionMapper.buildTransactionFromDepositDTO(depositDTO, creditorAccount));
    }

    List<TransactionDTO> pendingTransactionsDTO = new ArrayList<>();
    for (Transaction transaction : pendingTransactions) {
      pendingTransactionsDTO.add(TransactionMapper.buildTransactionDTO(transaction));
    }

    this.accountRepository.saveAll(creditorAccounts);
    this.transactionRepository.saveAll(pendingTransactions);
    this.kafkaTemplate.send("transaction_pato", pendingTransactionsDTO);
  }

  private Map<BigInteger, DepositDTO> mapAccountNumberToDeposit(List<DepositDTO> depositDTOs) {
    return depositDTOs.stream()
        .collect(Collectors.toMap(DepositDTO::getAccountNumber, depositDTO -> depositDTO));
  }

  public Account findAccountByAccountNumber(BigInteger accountNumber) {
    return this.accountRepository
        .findByAccountNumber(accountNumber)
        .orElseThrow(
            () ->
                new NotFoundException(
                    "No se ha encontrado una cuenta con el número " + accountNumber));
  }

  private List<Account> findAccountsByAccountNumbers(List<BigInteger> accountNumbers) {
    return this.accountRepository.findByAccountNumberIn(accountNumbers);
  }

  private void validateAccountFunds(Account account, BigDecimal amount)
      throws NotEnoughFundsException {
    boolean hasNotEnoughFunds = account.getBalance().compareTo(amount) < 0;
    if (hasNotEnoughFunds) {
      log.warn("La cuenta {} no tiene fondos suficientes.", account.getAccountNumber());
      throw new NotEnoughFundsException(
          "La cuenta " + account.getAccountNumber() + " no tiene fondos suficientes.");
    }
  }

  private BigDecimal obtainMontToPay(Account account, BigDecimal amount)
      throws NotEnoughFundsException {
    if(account.getBalance().compareTo(new BigDecimal(0)) < 0 || account.getBalance().compareTo(new BigDecimal(0)) == 0){
      log.warn("La cuenta {} no tiene fondos suficientes.", account.getAccountNumber());
      throw new NotEnoughFundsException(
          "La cuenta " + account.getAccountNumber() + " no tiene fondos suficientes.");
    }

    if(account.getBalance().compareTo(new BigDecimal(0)) > 0 && account.getBalance().compareTo(amount) < 0)
      return account.getBalance();
    else
      return amount;
  }

  private void validateWithdrawAmountAgainstFamily(BigDecimal amount, AccountFamily debtorFamily)
      throws TransactionException {
    boolean amountIsLowerThanMin = amount.compareTo(debtorFamily.getMinWithdrawalAmount()) < 0;
    if (amountIsLowerThanMin) {
      log.warn("El monto del retiro es menor que el mínimo permitido por la familia de cuentas.");
      throw new TransactionException(
          "El monto del retiro es menor que el mínimo permitido por la familia de cuentas.");
    }

    boolean amountIsGreaterThanMax = amount.compareTo(debtorFamily.getMaxWithdrawalAmount()) > 0;
    if (amountIsGreaterThanMax) {
      log.warn("El monto del retiro es mayor que el máximo permitido por la familia de cuentas.");
      throw new TransactionException(
          "El monto del retiro es mayor que el máximo permitido por la familia de cuentas.");
    }
  }

  private void validateDepositAmountAgainstFamily(BigDecimal amount, AccountFamily creditorFamily)
      throws TransactionException {
    boolean depositAmountIsGreaterThanMax =
        amount.compareTo(creditorFamily.getMaxDepositAmount()) > 0;
    if (depositAmountIsGreaterThanMax) {
      log.warn("El monto de depósito es mayor que el máximo permitido por la familia de cuentas.");
      throw new TransactionException(
          "El monto de depósito es mayor que el máximo permitido por la familia de cuentas.");
    }
  }

  private void withdrawAmountFromAccount(BigDecimal amount, Account account) {
    BigDecimal previousBalance = account.getBalance();
    BigDecimal newBalance = previousBalance.subtract(amount);

    account.setBalance(newBalance);

    this.accountRepository.save(account);
  }

  private void depositAmountIntoAccount(BigDecimal amount, Account account) {
    BigDecimal previousBalance = account.getBalance();
    BigDecimal newBalance = previousBalance.add(amount);

    account.setBalance(newBalance);

    this.accountRepository.save(account);
  }

  public List<Transaction> listAccountTransactions(BigInteger accountNumber) {
    Account companyAccount = findAccountByAccountNumber(accountNumber);
    return this.transactionRepository.findByCreditorAccountIdOrDebtorAccountId(
        companyAccount.getPk().getId(), companyAccount.getPk().getId());
  }

  public List<TransactionDTO> listAccountTransactionsDetails(BigInteger accountNumber) {
    Account companyAccount = findAccountByAccountNumber(accountNumber);

    List<Transaction> transactions =
        this.transactionRepository.findByCreditorAccountIdOrDebtorAccountId(
            companyAccount.getPk().getId(), companyAccount.getPk().getId());

    List<String> uniqueTransactionIds = this.prepareUniqueIdsFromTransactions(transactions);
    List<Account> involvedAccounts =
        this.accountRepository.findByPkCustomerGroupIdIn(uniqueTransactionIds);

    Map<String, Account> groupIdAccount = this.createAccountsMap(involvedAccounts);

    return transactions.stream()
        .map(
            transaction ->
                TransactionMapper.buildDetailsTransactionDTO(transaction, groupIdAccount))
        .collect(Collectors.toList());
  }

  private List<String> prepareUniqueIdsFromTransactions(List<Transaction> transactions) {
    List<String> involvedAccountIds =
        transactions.stream()
            .flatMap(
                transaction ->
                    Stream.of(
                        transaction.getCreditorGroupInternalId(),
                        transaction.getDebtorGroupInternalId()))
            .collect(Collectors.toList());

    return new ArrayList<>(new HashSet<>(involvedAccountIds));
  }

  private Map<String, Account> createAccountsMap(List<Account> accounts) {
    return accounts.stream().collect(Collectors.toMap(account -> account.getPk().getId(), o -> o));
  }
}
