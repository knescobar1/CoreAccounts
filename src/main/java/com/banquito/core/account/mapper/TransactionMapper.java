package com.banquito.core.account.mapper;

import com.banquito.cmpaymentscollections.payments.dto.DepositDTO;
import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.core.account.dto.WithdrawalDTO;
import com.banquito.core.account.enums.TransactionStateEnum;
import com.banquito.core.account.enums.TransactionTypeEnum;
import com.banquito.core.account.model.Account;
import com.banquito.core.account.model.Transaction;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionMapper {

  public static Transaction buildTransaction(TransactionDTO dto) {
    return Transaction.builder()
        .id(UUID.randomUUID().toString()) // Para crear una nueva transacción
        .creationDate(new Date()) // Para crear una nueva transacción
        .creditorGroupInternalId(dto.getCreditorGroupInternalId())
        .debtorGroupInternalId(dto.getDebtorGroupInternalId())
        .serviceLevel(dto.getServiceLevel())
        .amount(dto.getAmount())
        .state(dto.getState())
        .channel(dto.getChannel())
        .externalOperation(dto.getExternalOperation())
        .reference(dto.getReference())
        .documentNumber(dto.getDocumentNumber())
        .transactionNumber(dto.getTransactionNumber())
        .build();
  }

  public static TransactionDTO buildTransactionDTO(Transaction transaction) {
    return TransactionDTO.builder()
        .creditorGroupInternalId(transaction.getCreditorGroupInternalId())
        .debtorGroupInternalId(transaction.getDebtorGroupInternalId())
        .creationDate(transaction.getCreationDate())
        .serviceLevel(transaction.getServiceLevel())
        .amount(transaction.getAmount())
        .state(transaction.getState())
        .channel(transaction.getChannel())
        .externalOperation(transaction.getExternalOperation())
        .reference(transaction.getReference())
        .documentNumber(transaction.getDocumentNumber())
        .transactionNumber(transaction.getTransactionNumber())
        .build();
  }

  public static Transaction buildTransferTransaction(
      TransactionDTO dto, Account creditorAccount, Account debtorAccount) {
    return Transaction.builder()
        .creditorAccountId(creditorAccount.getPk().getId())
        .creditorAccountTypeId(creditorAccount.getPk().getTypeId())
        .creditorAccountFamilyId(creditorAccount.getPk().getFamilyId())
        .creditorGroupInternalId(creditorAccount.getPk().getCustomerGroupId())
        .debtorAccountId(debtorAccount.getPk().getId())
        .debtorAccountTypeId(debtorAccount.getPk().getTypeId())
        .debtorAccountFamilyId(debtorAccount.getPk().getFamilyId())
        .debtorGroupInternalId(debtorAccount.getPk().getCustomerGroupId())
        .serviceLevel(dto.getServiceLevel())
        .amount(dto.getAmount())
        .channel(dto.getChannel())
        .transactionType(TransactionTypeEnum.TRANSFER.getValue())
        .externalOperation(dto.getExternalOperation())
        .reference(dto.getReference())
        .state(TransactionStateEnum.EXECUTED.getValue())
        .documentNumber(dto.getDocumentNumber())
        .transactionNumber(dto.getTransactionNumber())
        .build();
  }

  public static Transaction buildWithdrawTransaction(
      WithdrawalDTO withdrawalDTO, Account debtorAccount) {

    return Transaction.builder()
        .id(UUID.randomUUID().toString())
        .debtorAccountId(debtorAccount.getPk().getId())
        .debtorAccountTypeId(debtorAccount.getPk().getTypeId())
        .debtorAccountFamilyId(debtorAccount.getPk().getFamilyId())
        .debtorGroupInternalId(debtorAccount.getPk().getCustomerGroupId())
        .creationDate(new Date())
        .amount(withdrawalDTO.getAmount())
        .state(TransactionStateEnum.EXECUTED.getValue())
        .channel(withdrawalDTO.getTransactionChannel())
        .transactionType(TransactionTypeEnum.WITHDRAWAL.getValue())
        .reference(withdrawalDTO.getNotes())
        .documentNumber(withdrawalDTO.getDocumentNumber())
        .transactionNumber(withdrawalDTO.getTransactionNumber())
        .build();
  }

  public static Transaction buildTransactionFromDepositDTO(
      DepositDTO depositDTO, Account creditorAccount) {
    return Transaction.builder()
        .id(UUID.randomUUID().toString())
        .creditorAccountId(creditorAccount.getPk().getId())
        .creditorAccountTypeId(creditorAccount.getPk().getTypeId())
        .creditorAccountFamilyId(creditorAccount.getPk().getFamilyId())
        .creditorGroupInternalId(creditorAccount.getPk().getCustomerGroupId())
        .creationDate(new Date())
        .amount(depositDTO.getAmount())
        .state(TransactionStateEnum.EXECUTED.getValue())
        .channel(depositDTO.getTransactionChannel())
        .transactionType(TransactionTypeEnum.DEPOSIT.getValue())
        .reference(depositDTO.getNotes())
        .documentNumber(depositDTO.getDocumentNumber())
        .transactionNumber(depositDTO.getTransactionNumber())
        .build();
  }

  public static TransactionDTO buildDetailsTransactionDTO(
      Transaction transaction, Map<String, Account> idAccounts) {
    TransactionDTO transactionDTO =
        TransactionDTO.builder()
            .creditorGroupInternalId(transaction.getCreditorGroupInternalId())
            .debtorGroupInternalId(transaction.getDebtorGroupInternalId())
            .creationDate(transaction.getCreationDate())
            .serviceLevel(transaction.getServiceLevel())
            .amount(transaction.getAmount())
            .state(transaction.getState())
            .channel(transaction.getChannel())
            .externalOperation(transaction.getExternalOperation())
            .reference(transaction.getReference())
            .documentNumber(transaction.getDocumentNumber())
            .transactionNumber(transaction.getTransactionNumber())
            .build();

    if (transaction.getCreditorAccountId() != null) {
      transactionDTO.setCreditorAccountNumber(
          idAccounts.get(transaction.getCreditorAccountId()).getAccountNumber());
    }

    if (transaction.getDebtorAccountId() != null) {
      transactionDTO.setDebtorAccountNumber(
          idAccounts.get(transaction.getDebtorAccountId()).getAccountNumber());
    }

    return transactionDTO;
  }
}
