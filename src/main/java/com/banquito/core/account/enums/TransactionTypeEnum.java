package com.banquito.core.account.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum TransactionTypeEnum {
  WITHDRAWAL("WIT", "Withdrawal"),
  DEPOSIT("DEP", "Deposit"),
  TRANSFER("TRA", "Transfer");

  private final String value;
  private final String text;
}
