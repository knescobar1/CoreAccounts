package com.banquito.core.account.dao;

import com.banquito.core.account.model.InterestSettings;
import com.banquito.core.account.model.InterestSettingsPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterestSettingsRepository
        extends JpaRepository<InterestSettings, InterestSettingsPK> {
}
