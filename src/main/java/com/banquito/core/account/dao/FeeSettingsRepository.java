package com.banquito.core.account.dao;

import com.banquito.core.account.model.FeeSettings;
import com.banquito.core.account.model.FeeSettingsPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeeSettingsRepository extends JpaRepository<FeeSettings, FeeSettingsPK> {}
