package com.banquito.core.account.mapper;

import com.banquito.core.account.dto.InterestSettingsDTO;
import com.banquito.core.account.model.InterestSettings;
import com.banquito.core.account.model.InterestSettingsPK;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InterestSettingsMapper {

  public static InterestSettings buildInterestSettings(InterestSettingsDTO dto) {

    return InterestSettings.builder()
        .pk(
            InterestSettingsPK.builder()
                .id(dto.getInterestId())
                .typeId(dto.getTypeId())
                .familyId(dto.getFamilyId())
                .build())
        .frecuency(dto.getFrecuency())
        .rate(dto.getRate())
        .rateTerms(dto.getRateTerms())
        .rateUnit(dto.getRateUnit())
        .spread(dto.getSpread())
        .build();
  }

  public static InterestSettingsDTO buildInterestSettingsDTO(InterestSettings interestSettings) {
    return InterestSettingsDTO.builder()
        .interestId(interestSettings.getPk().getId())
        .typeId(interestSettings.getPk().getTypeId())
        .familyId(interestSettings.getPk().getFamilyId())
        .frecuency(interestSettings.getFrecuency())
        .rate(interestSettings.getRate())
        .rateTerms(interestSettings.getRateTerms())
        .rateUnit(interestSettings.getRateUnit())
        .spread(interestSettings.getSpread())
        .build();
  }
}
