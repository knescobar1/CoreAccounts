package com.banquito.core.account.service;

import com.banquito.core.account.dao.AccountFamilyRepository;
import com.banquito.core.account.dao.AccountRepository;
import com.banquito.core.account.dao.AccountTypeRepository;
import com.banquito.core.account.dto.AccountDTO;
import com.banquito.core.account.enums.AccountStateEnum;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {
  @InjectMocks private AccountService accountService;

  @Mock private AccountRepository accountRepository;
  @Mock private AccountFamilyRepository accountFamilyRepository;
  @Mock private AccountTypeRepository accountTypeRepository;

  private Account account;
  private AccountType accountType;

  @BeforeEach
  void setUp() {
    AccountFamily accountFamily =
        AccountFamily.builder()
            .pk(new AccountFamilyPK("type1", "Family1"))
            .name("Ahorros")
            .holderType("Holder 1")
            .allowsCheckbook("T")
            .paysInterest("T")
            .interests(List.of())
            .fees(List.of())
            .taxes(List.of())
            .build();

    accountType = AccountType.builder().id("acc123").name("Cuenta").accrual("T").build();

    account =
        Account.builder()
            .pk(new AccountPK("", "", "", ""))
            .accountFamily(accountFamily)
            .accountNumber(BigInteger.valueOf(123_456))
            .balance(BigDecimal.valueOf(10_000))
            .state("")
            .build();
  }

  @Test
  void whenFindByAccountNumber_thenReturnShouldNotBeNull() {
    when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.of(account));
    when(accountTypeRepository.findById(any())).thenReturn(Optional.of(accountType));

    AccountDTO receivedAccount = accountService.findByAccountNumber(account.getAccountNumber());

    assertNotNull(receivedAccount);
  }

  @Test
  void whenFindByAccountNumberNotFound_thenRaiseNotFoundException() {
    when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.empty());

    BigInteger accountNumber = account.getAccountNumber();

    NotFoundException notFoundException =
        assertThrows(
            NotFoundException.class, () -> accountService.findByAccountNumber(accountNumber));

    assertNotNull(notFoundException);
  }

  @Test
  void whenFindGroupAccount_thenReturnShouldNotBeNull() {
    when(accountRepository.findByPkCustomerGroupIdAndAccountFamilyNameAndAccountNumber(
            any(), any(), any()))
        .thenReturn(Optional.of(account));

    Account actualAccount = accountService.findGroupAccount(any(), any(), any());

    assertNotNull(actualAccount);
  }

  @Test
  void whenFindGroupAccountNotFound_thenRaiseException() {
    when(accountRepository.findByPkCustomerGroupIdAndAccountFamilyNameAndAccountNumber(
            any(), any(), any()))
        .thenReturn(Optional.empty());

    NotFoundException notFoundException =
        assertThrows(
            NotFoundException.class, () -> accountService.findGroupAccount("", "", BigInteger.TEN));

    assertNotNull(notFoundException);
  }

  @Test
  void whenActivateAccount_thenFieldsShouldChange() {
    when(accountRepository.findByPkId(any())).thenReturn(Optional.of(account));

    accountService.activateAccount("");

    assertAll(
        () -> assertEquals(AccountStateEnum.ACTIVE.getValue(), account.getState()),
        () -> assertNotNull(account.getOpeningDate()),
        () -> assertNotNull(account.getLastModifiedDate()));
  }

  @Test
  void whenDisableAccount_thenFieldsShouldChange() {
    when(accountRepository.findByPkId(any())).thenReturn(Optional.of(account));

    accountService.disableAccount("");

    assertAll(
        () -> assertEquals(AccountStateEnum.INACTIVE.getValue(), account.getState()),
        () -> assertNotNull(account.getClosedDate()),
        () -> assertNotNull(account.getLastModifiedDate()));
  }

  @Test
  void listByGroup() {
    when(accountRepository.findByPkCustomerGroupId(any())).thenReturn(List.of());

    List<Account> accounts = accountService.listByGroup("");

    assertNotNull(accounts);
  }

  @Test
  void listByGroupAndStateActives() {
    when(accountRepository.findByPkCustomerGroupIdAndState(any(), any())).thenReturn(List.of());

    List<Account> accounts = accountService.listByGroupAndStateActives("");

    assertNotNull(accounts);
  }
}
