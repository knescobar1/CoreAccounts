package com.banquito.core.account.resource;

import com.banquito.cmpaymentscollections.payments.dto.DepositDTO;
import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.core.account.dto.WithdrawalDTO;
import com.banquito.core.account.mapper.TransactionMapper;
import com.banquito.core.account.model.Transaction;
import com.banquito.core.account.service.TransactionService;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionResource {
  private final TransactionService service;

  @PostMapping("/withdrawal")
  public ResponseEntity<TransactionDTO> makeWithdrawal(@RequestBody WithdrawalDTO dto) {
    try {
      Transaction withDrawTransaction = this.service.makeWithdrawal(dto);
      return ResponseEntity.ok(TransactionMapper.buildTransactionDTO(withDrawTransaction));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/deposit")
  public ResponseEntity<TransactionDTO> makeDeposit(@RequestBody DepositDTO dto) {
    try {
      Transaction depositTransaction = this.service.makeDeposit(dto);
      return ResponseEntity.ok(TransactionMapper.buildTransactionDTO(depositTransaction));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/deposit/multiple")
  public ResponseEntity<String> makeMultipleDeposits(@RequestBody List<DepositDTO> depositsDTO) {
    this.service.makeMultipleDeposits(depositsDTO);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/transfer")
  public ResponseEntity<TransactionDTO> makeTransaction(@RequestBody TransactionDTO dto) {
    Transaction transferTransaction = this.service.makeTransfer(dto);
    return ResponseEntity.ok(TransactionMapper.buildTransactionDTO(transferTransaction));
  }

  @GetMapping(path = "/{accountNumber}")
  public ResponseEntity<List<TransactionDTO>> listAccountTransactions(
      @PathVariable BigInteger accountNumber) {
    try {
      List<TransactionDTO> transactions =
          this.service.listAccountTransactions(accountNumber).stream()
              .map(TransactionMapper::buildTransactionDTO)
              .collect(Collectors.toList());
      return ResponseEntity.ok(transactions);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }
}
