package com.banquito.core.account.dao;


import com.banquito.core.account.model.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountTypeRepository extends JpaRepository<AccountType, String> {

}
