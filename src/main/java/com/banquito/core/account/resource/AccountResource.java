package com.banquito.core.account.resource;

import com.banquito.core.account.dto.AccountDTO;
import com.banquito.core.account.mapper.AccountMapper;
import com.banquito.core.account.model.Account;
import com.banquito.core.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/accounts")
@RequiredArgsConstructor
public class AccountResource {
  private final AccountService accountService;

  @GetMapping(path = "/accountNumber/{accountNumber}")
  public ResponseEntity<AccountDTO> findByAccountNumber(@PathVariable BigInteger accountNumber) {
    try {
      AccountDTO account = this.accountService.findByAccountNumber(accountNumber);
      return ResponseEntity.ok(account);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping("/search")
  public ResponseEntity<AccountDTO> findGroupAccount(
      @RequestParam String groupId, @RequestParam String type, @RequestParam BigInteger number) {
    Account account = this.accountService.findGroupAccount(groupId, type, number);
    return ResponseEntity.ok(AccountMapper.buildAccountDTO(account));
  }

  @PostMapping(path = "/create")
  public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountDTO dto) {
    Account account = this.accountService.create(AccountMapper.buildAccount(dto));
    return ResponseEntity.ok(AccountMapper.buildAccountDTO(account));
  }

  @PutMapping(path = "/activate/{accountId}")
  public ResponseEntity<Account> activateAccount(@PathVariable String accountId) {
    try {
      Account account = this.accountService.activateAccount(accountId);
      return ResponseEntity.ok(account);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping(path = "/disable/{accountId}")
  public ResponseEntity<Account> disableAccount(@PathVariable String accountId) {
    try {
      Account account = this.accountService.disableAccount(accountId);
      return ResponseEntity.ok(account);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/group/{groupId}")
  public ResponseEntity<List<AccountDTO>> listByGroup(@PathVariable String groupId) {
    List<AccountDTO> accountDTOs =
        this.accountService.listByGroup(groupId).stream()
            .map(AccountMapper::buildAccountDTO)
            .collect(Collectors.toList());
    return ResponseEntity.ok(accountDTOs);
  }

  @GetMapping(path = "/group/active/{groupId}")
  public ResponseEntity<List<AccountDTO>> listByGroupActives(@PathVariable String groupId) {
    List<AccountDTO> accountDTOs =
        this.accountService.listByGroupAndStateActives(groupId).stream()
            .map(AccountMapper::buildAccountDTO)
            .collect(Collectors.toList());
    return ResponseEntity.ok(accountDTOs);
  }
}
