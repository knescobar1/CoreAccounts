package com.banquito.core.account.dto;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TaxSettingsDTO {

  private String taxName;

  private String typeId;

  private String familyId;

  private BigDecimal rate;

  private String applicationFrecuency;

  private String trigger;

  private String formula;

  private Date applicationStartDate;
}
