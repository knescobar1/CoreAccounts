package com.banquito.core.account.mapper;

import com.banquito.core.account.dto.AccountFamilyDTO;
import com.banquito.core.account.dto.FeeSettingsDTO;
import com.banquito.core.account.dto.InterestSettingsDTO;
import com.banquito.core.account.dto.TaxSettingsDTO;
import com.banquito.core.account.model.AccountFamily;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountFamilyMapper {
  public static AccountFamilyDTO buildAccountFamilyDTO(AccountFamily accountFamily) {
    List<FeeSettingsDTO> feeDTOs =
        accountFamily.getFees().stream()
            .map(FeeSettingsMapper::buildFeeSettingsDTO)
            .collect(Collectors.toList());

    List<InterestSettingsDTO> interestDTOs =
        accountFamily.getInterests().stream()
            .map(InterestSettingsMapper::buildInterestSettingsDTO)
            .collect(Collectors.toList());

    List<TaxSettingsDTO> taxDTOs =
        accountFamily.getTaxes().stream()
            .map(TaxSettingsMapper::buildTaxSettingsDTO)
            .collect(Collectors.toList());

    return AccountFamilyDTO.builder()
        .familyId(accountFamily.getPk().getFamilyId())
        .name(accountFamily.getName())
        .holderType(accountFamily.getHolderType())
        .recommendedDepositAmount(accountFamily.getRecommendedDepositAmount())
        .maxDepositAmount(accountFamily.getMaxDepositAmount())
        .maxWithdrawalAmount(accountFamily.getMaxWithdrawalAmount())
        .minWithdrawalAmount(accountFamily.getMinWithdrawalAmount())
        .allowsCheckbook(accountFamily.getAllowsCheckbook())
        .allowsOverdraft(accountFamily.getAllowsOverdraft())
        .paysInterest(accountFamily.getPaysInterest())
        .currencies(accountFamily.getCurrencies())
        .memo(accountFamily.getMemo())
        .feeDTOs(feeDTOs)
        .interestDTOs(interestDTOs)
        .taxDTOs(taxDTOs)
        .build();
  }
}
