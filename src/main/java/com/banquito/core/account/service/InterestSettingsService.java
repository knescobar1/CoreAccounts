package com.banquito.core.account.service;

import com.banquito.core.account.dao.InterestSettingsRepository;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.model.InterestSettings;
import com.banquito.core.account.model.InterestSettingsPK;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InterestSettingsService {
  private final InterestSettingsRepository interestSettingsRepository;

  public InterestSettings create(InterestSettings interestSettings) {
    this.interestSettingsRepository.save(interestSettings);
    return interestSettings;
  }

  public InterestSettings getById(InterestSettingsPK interestSettingsPK) {
    Optional<InterestSettings> interestSettingsOpt =
        this.interestSettingsRepository.findById(interestSettingsPK);
    return interestSettingsOpt.orElseThrow(
        () -> new NotFoundException("The Interest type is not found"));
  }

  public InterestSettings modifySettings(InterestSettings interestSettings) {
    InterestSettings interestSettingsDb = this.getById(interestSettings.getPk());

    interestSettingsDb.setFrecuency(interestSettings.getFrecuency());
    interestSettingsDb.setRate(interestSettings.getRate());
    interestSettingsDb.setRateTerms(interestSettings.getRateTerms());
    interestSettingsDb.setRateUnit(interestSettings.getRateUnit());
    interestSettingsDb.setFrecuency(interestSettings.getFrecuency());
    interestSettingsDb.setSpread(interestSettings.getSpread());

    this.interestSettingsRepository.save(interestSettingsDb);
    return interestSettingsDb;
  }

  public void delete(InterestSettingsPK interestSettingsPK) {

    this.interestSettingsRepository.deleteById(interestSettingsPK);
  }
}
