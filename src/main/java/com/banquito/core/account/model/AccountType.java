package com.banquito.core.account.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ACC_ACCOUNT_TYPE")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class AccountType implements Serializable {

  private static final long serialVersionUID = 785744895856L;

  @Id
  @Column(name = "ACCOUNT_TYPE_ID", length = 32, nullable = false)
  @NonNull
  @EqualsAndHashCode.Include
  private String id;

  @Column(name = "NAME", length = 32, nullable = false)
  private String name;

  @Column(name = "ACCRUAL", length = 1, nullable = false)
  private String accrual;

  @Column(name = "TERM", length = 1, nullable = false)
  private String term;

  @Column(columnDefinition = "TEXT", name = "NOTES", nullable = true)
  private String notes;
}
