package com.banquito.core.account.resource;

import com.banquito.core.account.dto.AccountTypeDTO;
import com.banquito.core.account.mapper.AccountTypeMapper;
import com.banquito.core.account.model.AccountType;
import com.banquito.core.account.service.AccountTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/accounts")
@RequiredArgsConstructor
public class AccountTypeResource {

  private final AccountTypeService service;

  @GetMapping(path = "/{id}")
  public ResponseEntity<AccountTypeDTO> findById(@PathVariable String id) {
    try {
      return ResponseEntity.ok(AccountTypeMapper.buildAccountTypeDTO(this.service.findById(id)));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping
  public ResponseEntity<AccountTypeDTO> create(@RequestBody AccountTypeDTO dto) {
    try {
      AccountType accountType = this.service.create(AccountTypeMapper.buildAccountType(dto));
      return ResponseEntity.ok(AccountTypeMapper.buildAccountTypeDTO(accountType));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping
  public ResponseEntity<AccountTypeDTO> modify(@RequestBody AccountTypeDTO dto) {
    try {
      AccountType accountType = this.service.modify(AccountTypeMapper.buildAccountType(dto));
      return ResponseEntity.ok(AccountTypeMapper.buildAccountTypeDTO(accountType));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }
}
