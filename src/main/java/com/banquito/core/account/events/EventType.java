package com.banquito.core.account.events;

public enum EventType {
  CREATED,
  UPDATED,
  DELETED
}
