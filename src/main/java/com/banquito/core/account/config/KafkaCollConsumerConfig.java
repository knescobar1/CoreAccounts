package com.banquito.core.account.config;

import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@EnableKafka
@Configuration
public class KafkaCollConsumerConfig {

  @Bean
  public ConsumerFactory<String, TransactionDTO> consumerCollFactory() {
    Map<String, Object> config = new HashMap<>();
    // config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "34.125.80.201:9092");
    config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "34.125.33.174:9092");
    config.put(ConsumerConfig.GROUP_ID_CONFIG, "fooColl");
    config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
    config.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
    ObjectMapper om = new ObjectMapper();
    return new DefaultKafkaConsumerFactory<>(
        config, new StringDeserializer(), new JsonDeserializer<>(TransactionDTO.class));
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, TransactionDTO> fooCollListener() {
    ConcurrentKafkaListenerContainerFactory<String, TransactionDTO> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerCollFactory());
    return factory;
  }
}
