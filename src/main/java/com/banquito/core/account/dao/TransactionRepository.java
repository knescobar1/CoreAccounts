package com.banquito.core.account.dao;

import com.banquito.core.account.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, String> {

  List<Transaction> findByCreditorAccountId(String creditorAccount);

  List<Transaction> findByDebtorAccountId(String debtorAccount);

  List<Transaction> findByCreditorAccountIdOrDebtorAccountId(String creditorAccountId, String debtorAccountId);
}
