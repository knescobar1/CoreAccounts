package com.banquito.core.account.dto;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FeeSettingsDTO {

  private String feeName;

  private String typeId;

  private String familyId;

  private String calculationType;

  private String applicationFrecuency;

  private BigDecimal amount;

  private String trigger;

  private Date applicationStartDate;

  private String condition;

  private String formula;
}
