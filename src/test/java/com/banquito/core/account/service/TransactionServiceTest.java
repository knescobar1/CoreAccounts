package com.banquito.core.account.service;

import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.cmpaymentscollections.payments.dto.DepositDTO;
import com.banquito.core.account.dao.AccountRepository;
import com.banquito.core.account.dao.TransactionRepository;
import com.banquito.core.account.dto.WithdrawalDTO;
import com.banquito.core.account.exception.NotEnoughFundsException;
import com.banquito.core.account.exception.NotFoundException;
import com.banquito.core.account.exception.TransactionException;
import com.banquito.core.account.model.Account;
import com.banquito.core.account.model.AccountFamily;
import com.banquito.core.account.model.AccountFamilyPK;
import com.banquito.core.account.model.AccountPK;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {
  private static final BigDecimal MAX_DEPOSIT_AMOUNT = BigDecimal.valueOf(400);
  private static final BigDecimal MAX_WITHDRAWAL_AMOUNT = BigDecimal.valueOf(500);
  private static final BigDecimal MIN_WITHDRAWAL_AMOUNT = BigDecimal.valueOf(100);
  private static final BigDecimal INITIAL_DEBTOR_BALANCE = BigDecimal.valueOf(20_000);
  private static final BigDecimal INITIAL_CREDITOR_BALANCE = BigDecimal.valueOf(40_000);

  @InjectMocks private TransactionService transactionService;

  @Mock private AccountRepository accountRepository;
  @Mock private TransactionRepository transactionRepository;
  @Mock private KafkaTemplate<String, Object> kafkaTemplate;

  private Account debtorAccount, creditorAccount;
  private AccountFamily accountFamily;

  @BeforeEach
  void setUp() {
    accountFamily =
        AccountFamily.builder()
            .pk(new AccountFamilyPK("", ""))
            .maxDepositAmount(MAX_DEPOSIT_AMOUNT)
            .maxWithdrawalAmount(MAX_WITHDRAWAL_AMOUNT)
            .minWithdrawalAmount(MIN_WITHDRAWAL_AMOUNT)
            .build();

    debtorAccount =
        Account.builder()
            .pk(
                AccountPK.builder()
                    .id("123")
                    .typeId("type1")
                    .familyId("")
                    .customerGroupId("")
                    .build())
            .accountFamily(accountFamily)
            .accountNumber(BigInteger.valueOf(1234567890))
            .balance(INITIAL_DEBTOR_BALANCE)
            .build();

    creditorAccount =
        Account.builder()
            .pk(
                AccountPK.builder()
                    .id("123")
                    .typeId("type1")
                    .familyId("")
                    .customerGroupId("")
                    .build())
            .accountFamily(accountFamily)
            .accountNumber(BigInteger.valueOf(1234567890))
            .balance(INITIAL_CREDITOR_BALANCE)
            .build();
  }

  @Test
  void whenFindAccountNumber_thenAccountsShouldMatch() {
    when(accountRepository.findByAccountNumber(any()))
        .thenReturn(Optional.ofNullable(debtorAccount));

    assertEquals(
        debtorAccount,
        this.transactionService.findAccountByAccountNumber(BigInteger.valueOf(1234567890)));
  }

  @Test
  void whenFindAccountNumber_thenRaiseNotFoundException() {
    BigInteger debtorAccountNumber = BigInteger.valueOf(1234567890);

    when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.empty());

    Exception exception =
        assertThrows(
            NotFoundException.class,
            () -> this.transactionService.findAccountByAccountNumber(debtorAccountNumber));

    String actualMessage = exception.getMessage();

    assertNotNull(actualMessage);
  }

  @Test
  void whenMakeWithdrawal_thenDecreaseDebtorAccountBalance() {
    when(accountRepository.findByAccountNumber(any()))
        .thenReturn(Optional.ofNullable(debtorAccount));

    BigDecimal withdrawAmount = BigDecimal.valueOf(450);
    WithdrawalDTO withdrawalDTO =
        WithdrawalDTO.builder()
            .accountNumber(debtorAccount.getAccountNumber())
            .amount(withdrawAmount)
            .transactionChannel("")
            .notes("")
            .documentNumber("")
            .transactionNumber("")
            .build();

    this.transactionService.makeWithdrawal(withdrawalDTO);

    BigDecimal expectedBalance = INITIAL_DEBTOR_BALANCE.subtract(withdrawAmount);
    assertEquals(expectedBalance, debtorAccount.getBalance());
  }

  @Test
  void whenMakeDeposit_thenIncreaseCreditorAccountBalance() {
    when(accountRepository.findByAccountNumber(any()))
        .thenReturn(Optional.ofNullable(creditorAccount));

    BigDecimal depositAmount = BigDecimal.valueOf(300);
    DepositDTO depositDTO =
        DepositDTO.builder()
            .accountNumber(creditorAccount.getAccountNumber())
            .amount(depositAmount)
            .referenceId("")
            .notes("")
            .transactionChannel("")
            .documentNumber("")
            .transactionNumber("")
            .build();

    this.transactionService.makeDeposit(depositDTO);

    BigDecimal expectedBalance = INITIAL_CREDITOR_BALANCE.add(depositAmount);
    assertEquals(expectedBalance, creditorAccount.getBalance());
  }

  @Test
  void whenMakeTransferWithInsufficientFounds_thenRaiseNotEnoughFundsException() {
    prepareTransferTest();

    BigDecimal transferAmount = BigDecimal.valueOf(25_000); // Greater than initial balance
    TransactionDTO transaction = buildBaseTransaction();
    transaction.setAmount(transferAmount);

    Exception notEnoughFundsException =
        assertThrows(
            NotEnoughFundsException.class, () -> this.transactionService.makeTransfer(transaction));

    assertNotNull(notEnoughFundsException);
  }

  @Test
  void whenMakeTransactionWithMinAmount_thenRaiseTransactionException() {
    prepareTransferTest();

    BigDecimal transferLowerThanMinAmount = BigDecimal.valueOf(90);
    TransactionDTO transaction = buildBaseTransaction();
    transaction.setAmount(transferLowerThanMinAmount);

    Exception transactionMinException =
        assertThrows(
            TransactionException.class, () -> this.transactionService.makeTransfer(transaction));

    assertNotNull(transactionMinException);
  }

  @Test
  void whenMakeTransactionWithMaxAmount_thenRaiseTransactionException() {
    prepareTransferTest();

    BigDecimal transferGreaterThanMaxAmount = BigDecimal.valueOf(550);
    TransactionDTO transaction = buildBaseTransaction();
    transaction.setAmount(transferGreaterThanMaxAmount);

    Exception transactionMaxException =
        assertThrows(
            TransactionException.class, () -> this.transactionService.makeTransfer(transaction));

    assertNotNull(transactionMaxException);
  }

  @Test
  void whenMakeTransfer_thenDebtorBalanceShouldIncreaseAndCreditorDecrease() {
    prepareTransferTest();

    BigDecimal transferAmount = BigDecimal.valueOf(300);
    TransactionDTO transaction = buildBaseTransaction();
    transaction.setAmount(transferAmount);

    this.transactionService.makeTransfer(transaction);

    BigDecimal expectedDebtorBalance = BigDecimal.valueOf(19_700);
    BigDecimal expectedCreditorBalance = BigDecimal.valueOf(40_300);

    assertAll(
        () -> assertEquals(expectedDebtorBalance, debtorAccount.getBalance()),
        () -> assertEquals(expectedCreditorBalance, creditorAccount.getBalance()));
  }

  private void prepareTransferTest() {
    when(accountRepository.findByAccountNumber(any()))
        .thenReturn(Optional.of(creditorAccount))
        .thenReturn(Optional.of(debtorAccount));
  }

  private TransactionDTO buildBaseTransaction() {
    return TransactionDTO.builder()
        .creditorAccountNumber(creditorAccount.getAccountNumber())
        .debtorAccountNumber(debtorAccount.getAccountNumber())
        .build();
  }

  @Test
  void whenMakeMultipleDeposits_thenCreditorsBalanceShouldIncrease() {
    List<Account> creditorAccounts = prepareCreditorsAccounts();
    when(accountRepository.findByAccountNumberIn(any())).thenReturn(creditorAccounts);

    BigDecimal depositAmount = BigDecimal.valueOf(300);
    List<DepositDTO> deposits = new ArrayList<>();

    for (int i = 0; i < creditorAccounts.size(); i++) {
      deposits.add(
          DepositDTO.builder()
              .accountNumber(creditorAccounts.get(i).getAccountNumber())
              .amount(depositAmount)
              .build());
    }

    this.transactionService.makeMultipleDeposits(deposits);

    BigDecimal[] creditorBalances =
        creditorAccounts.stream().map(Account::getBalance).toArray(BigDecimal[]::new);

    BigDecimal[] expectedCreditorBalances =
        new BigDecimal[] {
          BigDecimal.valueOf(40_300), BigDecimal.valueOf(40_300),
        };

    assertArrayEquals(expectedCreditorBalances, creditorBalances);
  }

  private List<Account> prepareCreditorsAccounts() {
    Account account1 =
        Account.builder()
            .pk(
                AccountPK.builder()
                    .id("789")
                    .typeId("type1")
                    .familyId("")
                    .customerGroupId("")
                    .build())
            .accountFamily(accountFamily)
            .accountNumber(BigInteger.valueOf(1234567892))
            .balance(INITIAL_CREDITOR_BALANCE)
            .build();

    Account account2 =
        Account.builder()
            .pk(
                AccountPK.builder()
                    .id("987")
                    .typeId("type1")
                    .familyId("")
                    .customerGroupId("")
                    .build())
            .accountFamily(accountFamily)
            .accountNumber(BigInteger.valueOf(1234567893))
            .balance(INITIAL_CREDITOR_BALANCE)
            .build();

    return List.of(account1, account2);
  }
}
