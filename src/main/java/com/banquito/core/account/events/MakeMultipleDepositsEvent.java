package com.banquito.core.account.events;

import com.banquito.core.account.dto.DepositDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class MakeMultipleDepositsEvent extends Event<List<DepositDTO>> {}
