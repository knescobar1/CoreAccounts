package com.banquito.core.account.mapper;

import com.banquito.core.account.dto.TaxSettingsDTO;
import com.banquito.core.account.model.TaxSettings;
import com.banquito.core.account.model.TaxSettingsPK;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TaxSettingsMapper {

  public static TaxSettings buildTaxSettings(TaxSettingsDTO dto) {
    return TaxSettings.builder()
        .pk(
            TaxSettingsPK.builder()
                .taxName(dto.getTaxName())
                .typeId(dto.getTypeId())
                .familyId(dto.getFamilyId())
                .build())
        .rate(dto.getRate())
        .applicationFrecuency(dto.getApplicationFrecuency())
        .trigger(dto.getTrigger())
        .formula(dto.getFormula())
        .applicationStartDate(dto.getApplicationStartDate())
        .build();
  }

  public static TaxSettingsDTO buildTaxSettingsDTO(TaxSettings taxSettings) {
    return TaxSettingsDTO.builder()
        .taxName(taxSettings.getPk().getTaxName())
        .typeId(taxSettings.getPk().getTypeId())
        .familyId(taxSettings.getPk().getFamilyId())
        .rate(taxSettings.getRate())
        .applicationFrecuency(taxSettings.getApplicationFrecuency())
        .trigger(taxSettings.getTrigger())
        .formula(taxSettings.getFormula())
        .applicationStartDate(taxSettings.getApplicationStartDate())
        .build();
  }
}
